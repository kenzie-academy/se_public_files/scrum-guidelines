# Scrum Roles

## Product Owner
Create the project’s “Scrum Board”. Create columns from left to right: Open/Backlog, To Do, In Progress, QA/Testing, Done/Completed.

At least some of your tickets should be able to show that you are taking the clients interests in mind. But also know that you have a limited time to build an app that uses enough API endpoints to get full points for your team. So don't let your team spend too much time researching or designing layouts.

You will be creating most of the tickets. Some tickets may be created by other members of the team, but you get to decide which tickets are put into the backlog and how those tickets are prioritized in the backlog. No one else should be touching the backlog except you.

Not all tickets must be directly related to implementing a specific feature. Your team may find it helpful to create research tickets in order to better understand how to utilize certain technologies (specifically a component library) that would be useful to complete future tickets.

**As a PO, remember that any and all useful work done by your team should be recognized by a ticket.**


## Scrum Master

Make sure your repo is setup from the starter scaffolding. Add your team members as collaborators. 

Facilitate conversation with your team to understand how they like to work. Discuss pair programming vs. individual programming, working remote vs. in-office (if applicable), or other creative suggestions.

Establish a daily standup time that your team will follow for the duration of the project. Be prepared to lead the standup each day, asking each member to describe what changes they have made or discuss what they have learned, what they will be working on next, and any blockers/issues they have encountered. It is helpful to use the scrum board as a visual aid during standup.

Do not let PRs be merged before:
 1. Code review (ideally – but not always possible with time constraints)
 2. QA check

Although you are the sprint team leader, in general, *you will defer your interests in favor of the team's interests.* For instance, you could let other team members pick which tickets they want to work on first then you sweep up the rest. Another example: if a team member is struggling on a ticket you should be willing to drop whatever you are working on to help them out.

Code review is meant for code quality discussions. The back-and-forth nature of code review can sometimes take a lot of time. If the feature works, sometimes you will merge sloppy code anyways in order to meet a sprint deadline. Sloppy code (or code that does not follow "best practice") could be addressed separately in a future sprint. This type of ticket is usually called *technical debt.* Depending on the PO's priorities, it may be addressed quickly or it may sit in the backlog for awhile.

## QA Tester

During planning, it is your job to make sure the ticket has *clear acceptance criteria* before it gets put into the sprint. Acceptance criteria should be a set of numbered steps describing exactly how the feature is to be tested in the application.

Sometimes certain tickets are not QA’d, like research tickets and technical debt tickets. However, most tickets should be written/scoped in such a way so that they are testable – that is, they add a feature to the app that can be tested from the perspective of the user.

Screenshots or screen captures are a good way to show that QA has done its job. Good screenshots often include annotations such as a red circle or arrow which points out the specific element(s) in the screenshot that were tested. For more complex feature testing, such as those requiring button clicks or form submissions, you may find it easier to use screen recording software so you can document the full interaction. We have had success in the past with [Recordit](https://recordit.co/) or [ShareX](https://getsharex.com). 

If a code review will be done, don’t try to QA the feature until the code review is done. That way you do not waste time having to retest a feature if the code changes during the code review.

The best QA testers try to break things and discover hidden bugs in the features they are testing. They also take on the perspective of a new user, being able to provide 
feedback to the team about any unforeseen usability issues in the layout. For example, maybe the create new account button is really hard to find?

<!--- https://gitlab.com/-/ide/project/kenzie-academy/se_public_files/scrum-guidelines/edit/master/-/README.md -->
